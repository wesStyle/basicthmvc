package ru.wesstyle.thmvc.configuration;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Description;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;
import java.sql.SQLException;

@Configuration
@MapperScan("ru.wesstyle.thmvc.persistence")
public class DBConfiguration {

    @Bean
    @Description("Configuring Data Source")
    public DataSource dataSource() throws SQLException {
        //EmbeddedDatabase db = new EmbeddedDatabaseBuilder().setType(EmbeddedDatabaseType.HSQL).addScript("classpath:db/schema.sql").addScript("classpath:db/populate.sql").build();
        oracle.jdbc.pool.OracleDataSource db = new oracle.jdbc.pool.OracleDataSource();
        //db.setURL("jdbc:oracle:thin:@//192.168.11.123:1521/orcl?useUnicode=yes");
        db.setURL("jdbc:oracle:thin:wesStyle/carbon@10.37.129.3:1521:orcl");
        //db.setUser("wesStyle");
        //db.setPassword("carbon");
        return db;
    }

    @Bean
    @Description("Configuring SqlSessionFactory")
    public SqlSessionFactory sqlSessionFactory() throws Exception {
        SqlSessionFactoryBean ss = new SqlSessionFactoryBean();
        ss.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath:persistence/*.xml"));
        ss.setDataSource(dataSource());
        ss.setTypeAliasesPackage("ru.wesstyle.thmvc.domain");
        return ss.getObject();
    }

    @Bean
    @Description("Configuring Transaction Manager")
    public DataSourceTransactionManager dataSourceTransactionManager() throws SQLException {
        DataSourceTransactionManager dt = new DataSourceTransactionManager();
        dt.setDataSource(dataSource());
        return dt;
    }
}
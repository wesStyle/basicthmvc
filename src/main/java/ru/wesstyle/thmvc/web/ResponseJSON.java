package ru.wesstyle.thmvc.web;

/**
 * Created by wesStyle on 19/03/14.
 */
public class ResponseJSON {

    String status;
    String html;

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}

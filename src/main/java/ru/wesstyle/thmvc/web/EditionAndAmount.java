package ru.wesstyle.thmvc.web;

import java.io.Serializable;

/**
 * Created by wesStyle on 22/03/14.
 */
public class EditionAndAmount implements Serializable {
    private static final long serialVersionUID = 5467713262068198522L;

    int edId;
    int amount;
    String type;

    public int getEdId() {
        return edId;
    }

    public void setEdId(int edId) {
        this.edId = edId;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}

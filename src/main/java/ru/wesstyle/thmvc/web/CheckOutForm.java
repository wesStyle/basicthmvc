package ru.wesstyle.thmvc.web;

import java.io.Serializable;

/**
 * Created by wesStyle on 24/03/14.
 */
public class CheckOutForm implements Serializable {
    private static final long serialVersionUID = -473746229209965547L;

    String pmRadio;

    String newPmRadio;

    String newPsName;
    String newPsWallet;

    String newCrdName;
    String newCrdNumber;
    int newCrdExpYear;
    int newCrdExpMonth;
    String newCrdSecNumber;

    int orderId;

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    String crdMulti;
    String psMulti;

    public int getNewCrdExpYear() {
        return newCrdExpYear;
    }

    public void setNewCrdExpYear(int newCrdExpYear) {
        this.newCrdExpYear = newCrdExpYear;
    }

    public int getNewCrdExpMonth() {
        return newCrdExpMonth;
    }

    public void setNewCrdExpMonth(int newCrdExpMonth) {
        this.newCrdExpMonth = newCrdExpMonth;
    }

    public String getNewCrdSecNumber() {
        return newCrdSecNumber;
    }

    public void setNewCrdSecNumber(String newCrdSecNumber) {
        this.newCrdSecNumber = newCrdSecNumber;
    }

    public String getPmRadio() {
        return pmRadio;
    }

    public void setPmRadio(String pmRadio) {
        this.pmRadio = pmRadio;
    }

    public String getNewPmRadio() {
        return newPmRadio;
    }

    public void setNewPmRadio(String newPmRadio) {
        this.newPmRadio = newPmRadio;
    }

    public String getNewPsName() {
        return newPsName;
    }

    public void setNewPsName(String newPsName) {
        this.newPsName = newPsName;
    }

    public String getNewPsWallet() {
        return newPsWallet;
    }

    public void setNewPsWallet(String newPsWallet) {
        this.newPsWallet = newPsWallet;
    }

    public String getNewCrdName() {
        return newCrdName;
    }

    public void setNewCrdName(String newCrdName) {
        this.newCrdName = newCrdName;
    }

    public String getNewCrdNumber() {
        return newCrdNumber;
    }

    public void setNewCrdNumber(String newCrdNumber) {
        this.newCrdNumber = newCrdNumber;
    }

    public String getCrdMulti() {
        return crdMulti;
    }

    public void setCrdMulti(String crdMulti) {
        this.crdMulti = crdMulti;
    }

    public String getPsMulti() {
        return psMulti;
    }

    public void setPsMulti(String psMulti) {
        this.psMulti = psMulti;
    }
}

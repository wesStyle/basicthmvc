package ru.wesstyle.thmvc.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;
import org.thymeleaf.fragment.ElementAndAttributeNameFragmentSpec;
import org.thymeleaf.fragment.IFragmentSpec;
import ru.wesstyle.thmvc.domain.ChangeAmountParameters;
import ru.wesstyle.thmvc.domain.CreateSBParameters;
import ru.wesstyle.thmvc.domain.DeleteSbParameters;
import ru.wesstyle.thmvc.domain.order.ShoppingCart;
import ru.wesstyle.thmvc.domain.order.ShoppingItem;
import ru.wesstyle.thmvc.domain.software.Edition;
import ru.wesstyle.thmvc.service.IOrderService;
import ru.wesstyle.thmvc.service.ISoftwareService;
import ru.wesstyle.thmvc.service.IUserService;
import ru.wesstyle.thmvc.util.ShoppingCartUtil;
import ru.wesstyle.thmvc.web.EditionAndAmount;
import ru.wesstyle.thmvc.web.ResponseJSON;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by wesStyle on 21/03/14.
 */
@Controller
@RequestMapping("/cart")
public class ShoppingCartController {
    @Autowired
    ISoftwareService softwareService;

    @Autowired
    CatalogController catalogController;

    @Autowired
    IUserService userService;

    @Autowired
    IOrderService orderService;

    @Autowired
    TemplateEngine templateEngine;

    @Autowired
    ServletContext context;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView showCart(ModelAndView model, HttpSession httpSession) {
        model.setViewName("cart");
        catalogController.addNewCartToSession(httpSession);
        ShoppingCart cartd = (ShoppingCart) httpSession.getAttribute("cartsd");
        insertAvailability(cartd);
        model.addObject("cartsd", cartd);
        return model;
    }

    @RequestMapping(value = "/ref", method = RequestMethod.POST)
    @ResponseBody
    public ResponseJSON refreshCart(@RequestBody List<EditionAndAmount> eaas, HttpSession httpSession, HttpServletRequest request, HttpServletResponse response) {
        ResponseJSON responseJSON = new ResponseJSON();
        catalogController.addNewCartToSession(httpSession);
        ShoppingCart cartd = (ShoppingCart) httpSession.getAttribute("cartsd");

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth.getPrincipal().toString().equals("anonymousUser")) {
            ShoppingCartUtil.refreshCart(cartd, eaas);
            insertAvailability(cartd);
        } else {
            refreshCarts(eaas, cartd, false);
        }

        responseJSON.setStatus("SUCCESS");
        return responseJSON;
    }

    void refreshCarts(List<EditionAndAmount> eaas, ShoppingCart cartd, boolean inc) {
        int amnt;
        for (Iterator<EditionAndAmount> iterator = eaas.iterator(); iterator.hasNext(); ) {
            EditionAndAmount eaa = iterator.next();
            Edition edition = softwareService.getEditionById(eaa.getEdId());
            if (eaa.getType().equals("Цифровое"))
                if (cartd.hasItem(edition)) {
                    if (eaa.getAmount() == 0) {
                        DeleteSbParameters parameters = new DeleteSbParameters(cartd.getItemId(edition), cartd.getId());
                        orderService.deleteCartItem(parameters);
                    }
                    if (inc) amnt = cartd.getItemByEdId(eaa.getEdId()).getAmount() + eaa.getAmount();
                    else amnt = eaa.getAmount();
                    ChangeAmountParameters parameters = new ChangeAmountParameters(cartd.getItemId(edition), edition.getId(), amnt, cartd.getId());
                    orderService.changeCartItem(parameters);
                } else {
                    CreateSBParameters parameters = new CreateSBParameters(cartd.getUserId(), edition.getId(), eaa.getAmount());
                    orderService.addToCart(parameters);
                }
        }
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ResponseBody
    public ResponseJSON addItem(@RequestBody EditionAndAmount eaa, HttpSession httpSession, HttpServletRequest request, HttpServletResponse response) {
        ResponseJSON responseJSON = new ResponseJSON();
        catalogController.addNewCartToSession(httpSession);
        try {
            Edition edition = softwareService.getEditionById(eaa.getEdId());
            if (edition == null) throw new Exception("Edition not found");

            catalogController.addNewCartToSession(httpSession);
            ShoppingCart cartd = (ShoppingCart) httpSession.getAttribute("cartsd");

            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            if (auth.getPrincipal().toString().equals("anonymousUser")) {
                if (eaa.getType().equals("Цифровое"))
                    cartd.addShoppingItem(edition, eaa.getAmount());
            } else {
                List<EditionAndAmount> list = new ArrayList<EditionAndAmount>();
                list.add(eaa);
                refreshCarts(list, cartd, true);
            }

            catalogController.addNewCartToSession(httpSession);

            cartd = (ShoppingCart) httpSession.getAttribute("cartsd");

            final WebContext ctx = new WebContext(request, response, context);
            ctx.setVariable("cartsd", cartd);

            IFragmentSpec fragmentSpec = new ElementAndAttributeNameFragmentSpec(null, "th:fragment", "cart-navbar", true);
            responseJSON.setHtml(templateEngine.process("cart-nav", ctx, fragmentSpec));

            responseJSON.setStatus("SUCCESS");
        } catch (Exception e) {
            responseJSON.setStatus("FAIL");
        }
        return responseJSON;
    }

    public void insertAvailability(ShoppingCart shoppingCart) {
        for (Iterator<ShoppingItem> iterator = shoppingCart.getList().iterator(); iterator.hasNext(); ) {
            ShoppingItem item = iterator.next();
            item.setAvailable(orderService.checkAvailability(item.getEd().getId(), item.getAmount()));
            if (item.getAvailable() > 0) shoppingCart.setHasNotAvailableItems(true);
        }
    }
}

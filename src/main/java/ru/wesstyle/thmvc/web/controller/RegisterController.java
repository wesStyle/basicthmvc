package ru.wesstyle.thmvc.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.wesstyle.thmvc.domain.RegisterUserParameters;
import ru.wesstyle.thmvc.domain.order.ShoppingCart;
import ru.wesstyle.thmvc.service.UserService;
import ru.wesstyle.thmvc.web.RegisterForm;

import javax.servlet.http.HttpSession;

/**
 * Created by wesStyle on 06/04/14.
 */

@Controller
@RequestMapping("/register")
public class RegisterController {

    @Autowired
    UserService userService;

    @Autowired
    CatalogController catalogController;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView showRegForm(ModelAndView model, HttpSession httpSession) {
        catalogController.addNewCartToSession(httpSession);
        ShoppingCart cartd = (ShoppingCart) httpSession.getAttribute("cartsd");
        model.addObject("cartsd", cartd);
        RegisterForm registerForm = new RegisterForm();
        model.addObject("regForm", registerForm);
        model.setViewName("register");
        return model;
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ModelAndView registerUser(RegisterForm form, ModelAndView model, HttpSession httpSession) {
        boolean hasErrors = false;
        boolean userExists = false;
        if (!form.getPassword1().equals(form.getPassword2())) {
            hasErrors = true;
            model.addObject("hasErrors", hasErrors);
            return showRegForm(model, httpSession);
        }

        int uid = 0;
        RegisterUserParameters parameters = new RegisterUserParameters(
                form.getLogin(),
                form.getName(),
                form.getSurname(),
                form.getEmail(),
                form.getPassword1(),
                "ROLE_USER",
                uid
        );

        userService.regUser(parameters);

        if (uid == -1) {
            userExists = true;
            model.addObject("userExists", userExists);
            return showRegForm(model, httpSession);
        } else if (uid > 0)
            model.setViewName("redirect:reg-success");
        else model.setViewName("redirect:/");
        return model;
    }

    @RequestMapping("/reg-success")
    public ModelAndView showRegSuccess(ModelAndView model, HttpSession httpSession) {
        catalogController.addNewCartToSession(httpSession);
        ShoppingCart cartd = (ShoppingCart) httpSession.getAttribute("cartsd");
        model.addObject("cartsd", cartd);
        model.setViewName("reg-success");
        return model;
    }
}

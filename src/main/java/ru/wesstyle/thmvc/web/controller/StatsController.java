package ru.wesstyle.thmvc.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.wesstyle.thmvc.domain.OrderLogItem;
import ru.wesstyle.thmvc.domain.order.ShoppingCart;
import ru.wesstyle.thmvc.service.ILogService;
import ru.wesstyle.thmvc.service.IOrderService;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by wesStyle on 07/04/14.
 */
@Controller
@RequestMapping("/stats")
public class StatsController {

    @Autowired
    CatalogController catalogController;

    @Autowired
    IOrderService orderService;

    @Autowired
    ILogService logService;

    @RequestMapping("/")
    public ModelAndView showStats(ModelAndView model, HttpSession httpSession) {
        catalogController.addNewCartToSession(httpSession);
        ShoppingCart cartd = (ShoppingCart) httpSession.getAttribute("cartsd");
        model.addObject("cartsd", cartd);

        List<OrderLogItem> logs = logService.getOrderLogs();
        int ordersCount = logService.getOrdersCount();
        int doneOrdersCount = logService.getDoneOrdersCount();
        int delayedOrdersCount = logService.getDelayedOrdersCount();
        int usersCount = logService.getUsersCount();
        model.addObject("users", usersCount);
        model.addObject("ods", ordersCount);
        model.addObject("done", doneOrdersCount);
        model.addObject("delayed", delayedOrdersCount);
        model.addObject("logs", logs);
        model.setViewName("stats");
        return model;
    }

    @RequestMapping("/processOrders")
    public ModelAndView processorders(ModelAndView model, HttpSession httpSession) {
        orderService.processOrders();
        return showStats(model, httpSession);
    }
}

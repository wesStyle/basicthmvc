package ru.wesstyle.thmvc.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.wesstyle.thmvc.domain.Customer;
import ru.wesstyle.thmvc.domain.order.ShoppingCart;
import ru.wesstyle.thmvc.domain.software.Category;
import ru.wesstyle.thmvc.domain.software.Software;
import ru.wesstyle.thmvc.service.IOrderService;
import ru.wesstyle.thmvc.service.ISoftwareService;
import ru.wesstyle.thmvc.service.IUserService;
import ru.wesstyle.thmvc.util.CategoryUtil;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;

/**
 * Created by wesStyle on 14/03/14.
 */
@Controller
@RequestMapping("/catalog")
public class CatalogController {
    private static final String HOME_VIEW = "home";
    private static final String DETAILS_VIEW = "details";

    @Autowired
    IOrderService orderService;

    @Autowired
    IUserService userService;

    @Autowired
    ShoppingCartController cartController;

    @Autowired
    ISoftwareService softwareService;

    @Autowired
    ConversionService conversionService;

    @RequestMapping("/{parentId}/{catId}/{sftId}")
    public ModelAndView showDetails(@PathVariable String parentId, @PathVariable String catId, @PathVariable String sftId, ModelAndView model, HttpSession httpSession) {
        int catid = 0;
        int pid = 0;
        int sftid = 0;
        try {
            catid = Integer.parseInt(catId);
            pid = Integer.parseInt(parentId);
            sftid = Integer.parseInt(sftId);
        } catch (NumberFormatException ex) {
            return showHome(model, httpSession);
        }
        ArrayList<Category> cats = softwareService.getSoftWareCategories();
        Category pcat = CategoryUtil.isParentCategoryExist(pid, cats);
        if (pcat == null) return showHome(model, httpSession);
        Category cat = CategoryUtil.isCategoryExist(catid, cats);
        if (cat == null) return showHome(model, httpSession);
        Software sft = softwareService.getSoftware(sftid);
        if (sft == null) return showHome(model, httpSession);
        model.addObject("soft", sft);
        model.addObject("catId", cat);
        model.addObject("pId", pcat);
        model.addObject("cats", cats);
        model.setViewName(DETAILS_VIEW);
        addNewCartToSession(httpSession);
        model.addObject("cartsd", httpSession.getAttribute("cartsd"));
        return model;
    }

    @RequestMapping("/{parentId}/{catId}")
    public ModelAndView showHomeCat(@PathVariable String parentId, @PathVariable String catId, ModelAndView model, HttpSession httpSession) {
        int catid = 0;
        int pid = 0;
        try {
            catid = Integer.parseInt(catId);
            pid = Integer.parseInt(parentId);
        } catch (NumberFormatException ex) {
            return showHome(model, httpSession);
        }
        ArrayList<Category> cats = softwareService.getSoftWareCategories();
        Category pcat = CategoryUtil.isParentCategoryExist(pid, cats);
        if (pcat == null) return showHome(model, httpSession);
        Category cat = CategoryUtil.isCategoryExist(catid, cats);
        if (cat == null) return showHome(model, httpSession);

        model.addObject("soft", softwareService.getSoftwareByCategoryId(catid));
        model.addObject("catId", cat);
        model.addObject("pId", pcat);
        model.addObject("cats", cats);
        addNewCartToSession(httpSession);
        model.addObject("cartsd", httpSession.getAttribute("cartsd"));
        model.setViewName(HOME_VIEW);
        return model;
    }

    @RequestMapping("/{parentId}")
    public ModelAndView showHomeParentCat(@PathVariable String parentId, ModelAndView model, HttpSession httpSession) {
        int pid = 0;
        try {
            pid = Integer.parseInt(parentId);
        } catch (NumberFormatException ex) {
            return showHome(model, httpSession);
        }
        ArrayList<Category> cats = softwareService.getSoftWareCategories();
        Category pcat = CategoryUtil.isParentCategoryExist(pid, cats);
        if (pcat == null) return showHome(model, httpSession);

        Category cat = new Category();
        cat.setId(0);
        model.addObject("soft", softwareService.getSoftwareByParentCategoryId(pid));
        model.addObject("catId", cat);
        model.addObject("pId", pcat);
        model.addObject("cats", cats);
        addNewCartToSession(httpSession);
        model.addObject("cartsd", httpSession.getAttribute("cartsd"));
        model.setViewName(HOME_VIEW);
        return model;
    }

    @RequestMapping("/")
    public ModelAndView showHome(ModelAndView model, HttpSession httpSession) {
        model.setViewName(HOME_VIEW);
        model.addObject("cats", softwareService.getSoftWareCategories());
        Category cat = new Category();
        cat.setId(0);
        Category pcat = new Category();
        pcat.setId(0);
        model.addObject("catId", cat);
        model.addObject("pId", pcat);
        ArrayList<Software> sft = softwareService.getSoftwareList();
        model.addObject("soft", sft);
        addNewCartToSession(httpSession);
        model.addObject("cartsd", httpSession.getAttribute("cartsd"));
        return model;
    }

    public void addNewCartToSession(HttpSession httpSession) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth.getPrincipal().toString().equals("anonymousUser")) {
            ShoppingCart cartd = (ShoppingCart) httpSession.getAttribute("cartsd");
            if (cartd == null) {
                cartd = new ShoppingCart();
                httpSession.setAttribute("cartsd", cartd);
            }
        } else {
            Customer user = userService.getUser(auth.getName());
            user.setPaymentMethodList(userService.getUserPms(user.getUsername()));
            orderService.createOpenOrders(user.getId());

            ShoppingCart cartd = orderService.getDigitalOpenOrder(user.getId());
            cartd.setList(orderService.populateCart(cartd.getId()));
            cartd.setCount();

            cartController.insertAvailability(cartd);
            httpSession.setAttribute("cartsd", cartd);
        }
    }
}

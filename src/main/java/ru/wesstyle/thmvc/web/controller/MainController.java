package ru.wesstyle.thmvc.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

/**
 * Created by wesStyle on 21/03/14.
 */
@Controller
public class MainController {
    @Autowired
    AboutController aboutController;

    @RequestMapping("/")
    public ModelAndView showCatalog(ModelAndView model, HttpSession httpSession) {
        return aboutController.showAbout(model, httpSession);
    }
}

package ru.wesstyle.thmvc.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.wesstyle.thmvc.domain.order.ShoppingCart;

import javax.servlet.http.HttpSession;

/**
 * Created by wesStyle on 07/04/14.
 */
@Controller
@RequestMapping("/about")
public class AboutController {
    @Autowired
    CatalogController catalogController;

    @RequestMapping("/")
    public ModelAndView showAbout(ModelAndView model, HttpSession httpSession) {
        catalogController.addNewCartToSession(httpSession);
        ShoppingCart cartd = (ShoppingCart) httpSession.getAttribute("cartsd");
        model.addObject("cartsd", cartd);

        model.setViewName("about");
        return model;
    }
}

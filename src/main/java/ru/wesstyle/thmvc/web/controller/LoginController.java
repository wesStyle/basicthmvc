package ru.wesstyle.thmvc.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;
import org.thymeleaf.fragment.ElementAndAttributeNameFragmentSpec;
import org.thymeleaf.fragment.IFragmentSpec;
import ru.wesstyle.thmvc.domain.ChangeAmountParameters;
import ru.wesstyle.thmvc.domain.CreateSBParameters;
import ru.wesstyle.thmvc.domain.Customer;
import ru.wesstyle.thmvc.domain.order.ShoppingCart;
import ru.wesstyle.thmvc.domain.order.ShoppingItem;
import ru.wesstyle.thmvc.service.IOrderService;
import ru.wesstyle.thmvc.service.IUserService;
import ru.wesstyle.thmvc.web.ResponseJSON;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Iterator;

/**
 * Created by wesStyle on 21/03/14.
 */
@Controller
public class LoginController {
    private static final String LOGIN_VIEW = "login";
    private static final String LOGOUT_VIEW = "logout";

    @Autowired
    TemplateEngine templateEngine;

    @Autowired
    IOrderService orderService;

    @Autowired
    ServletContext context;

    @Autowired
    IUserService userService;

    @Autowired
    @Qualifier("authenticationManagerBean")
    AuthenticationManager authenticationManager;

    @RequestMapping("/login")
    public String login() {
        return LOGIN_VIEW;
    }

    @RequestMapping("/login-error")
    public String loginError(Model model) {
        model.addAttribute("loginError", "true");
        return LOGIN_VIEW;
    }

    @RequestMapping(value = "/logins", method = RequestMethod.GET)
    public String sendLoginForm() {
        return LOGIN_VIEW;
    }

    @RequestMapping(value = "/logins", method = RequestMethod.POST)
    @ResponseBody
    public ResponseJSON performLogin(
            @RequestParam("username") String username,
            @RequestParam("password") String password,
            HttpSession httpSession,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {

        ResponseJSON lj = new ResponseJSON();
        try {
            Authentication token = new UsernamePasswordAuthenticationToken(username, password);
            Authentication auth = authenticationManager.authenticate(token);
            SecurityContext securityContext = SecurityContextHolder.getContext();
            securityContext.setAuthentication(auth);
            lj.setStatus("SUCCESS");
            Customer user = userService.getUser(username);
            user.setPaymentMethodList(userService.getUserPms(user.getUsername()));
            ShoppingCart cartd = (ShoppingCart) httpSession.getAttribute("cartsd");

            mergeCarts(cartd, user.getId());

            httpSession.removeAttribute("cartsd");
        } catch (AuthenticationException ex) {
            final WebContext ctx = new WebContext(request, response, context);
            ctx.setVariable("loginError", "true");
            lj.setStatus("FAIL");
            IFragmentSpec fragmentSpec = new ElementAndAttributeNameFragmentSpec(null, "th:fragment", "modal-form", true);
            lj.setHtml(templateEngine.process("loginform", ctx, fragmentSpec));
        }
        return lj;
    }

    private void mergeCarts(ShoppingCart cartd, int uid) {
        orderService.createOpenOrders(uid);

        ShoppingCart cartdU = orderService.getDigitalOpenOrder(uid);
        cartdU.setList(orderService.populateCart(cartdU.getId()));
        cartdU.setCount();


        merge(cartd, cartdU, uid);
    }

    private void merge(ShoppingCart from, ShoppingCart to, int uid) {
        boolean found;
        for (Iterator<ShoppingItem> iterator = from.getList().iterator(); iterator.hasNext(); ) {
            ShoppingItem item = iterator.next();
            found = false;
            for (Iterator<ShoppingItem> iteratorTo = to.getList().iterator(); iteratorTo.hasNext(); ) {
                ShoppingItem itemTo = iteratorTo.next();
                if (item.getEd().getId() == itemTo.getEd().getId()) {
                    ChangeAmountParameters parameters = new ChangeAmountParameters(itemTo.getId(), itemTo.getEd().getId(), item.getAmount() + itemTo.getAmount(), to.getId());
                    orderService.changeCartItem(parameters);
                    found = true;
                    break;
                }
            }
            if (!found) {
                CreateSBParameters parameters = new CreateSBParameters(uid, item.getEd().getId(), item.getAmount());
                orderService.addToCart(parameters);
            }
        }
    }
}

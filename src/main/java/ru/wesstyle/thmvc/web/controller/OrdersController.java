package ru.wesstyle.thmvc.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.wesstyle.thmvc.domain.Customer;
import ru.wesstyle.thmvc.domain.order.ShoppingCart;
import ru.wesstyle.thmvc.domain.order.ShoppingItem;
import ru.wesstyle.thmvc.service.IOrderService;
import ru.wesstyle.thmvc.service.IUserService;

import javax.servlet.http.HttpSession;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by wesStyle on 24/03/14.
 */
@Controller
@RequestMapping("/orders")
public class OrdersController implements Serializable {
    private static final long serialVersionUID = -7977415746883096143L;

    @Autowired
    CatalogController catalogController;

    @Autowired
    IUserService userService;

    @Autowired
    IOrderService orderService;

    @RequestMapping("/{id}")
    public ModelAndView showOrder(ModelAndView model, @PathVariable String id, HttpSession httpSession) {
        catalogController.addNewCartToSession(httpSession);
        ShoppingCart cartd = (ShoppingCart) httpSession.getAttribute("cartsd");
        model.addObject("cartsd", cartd);

        model.setViewName("orderPage");

        int idn;
        try {
            idn = Integer.parseInt(id);
        } catch (NumberFormatException ex) {
            model.setViewName("orders");
            return model;
        }

        ShoppingCart order = orderService.getOrder(idn);
        order.setList(orderService.populateCart(order.getId()));
        populateDoneOrders(order);

        if (order == null) {
            model.setViewName("orders");
            return model;
        }
        model.addObject("cart", order);
        return model;
    }

    @RequestMapping("/")
    public ModelAndView showOrders(ModelAndView model, HttpSession httpSession) {
        catalogController.addNewCartToSession(httpSession);
        ShoppingCart cartd = (ShoppingCart) httpSession.getAttribute("cartsd");
        model.addObject("cartsd", cartd);

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        Customer user = userService.getUser(auth.getName());
        user.setPaymentMethodList(userService.getUserPms(user.getUsername()));
        ArrayList<ShoppingCart> list = orderService.getAllOrders(user.getId());

        model.addObject("list", list);
        model.setViewName("orders");
        return model;
    }


    private void populateDoneOrders(ShoppingCart sc) {
        if (sc.getStatus().equals("Оформлено")) {
            sc.setList(orderService.populateCart(sc.getId()));
            for (Iterator<ShoppingItem> iterator1 = sc.getList().iterator(); iterator1.hasNext(); ) {
                ShoppingItem item = iterator1.next();
                item.setKeys(orderService.getKeys(sc.getId(), item.getEd().getId()));
            }
        }

    }
}

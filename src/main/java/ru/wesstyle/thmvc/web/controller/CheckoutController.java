package ru.wesstyle.thmvc.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import ru.wesstyle.thmvc.domain.*;
import ru.wesstyle.thmvc.domain.order.ShoppingCart;
import ru.wesstyle.thmvc.service.IOrderService;
import ru.wesstyle.thmvc.service.IUserService;
import ru.wesstyle.thmvc.web.CheckOutForm;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;

/**
 * Created by wesStyle on 24/03/14.
 */
@Controller
@RequestMapping("/checkout")
public class CheckoutController {
    @Autowired
    CatalogController catalogController;

    @Autowired
    ShoppingCartController cartController;

    @Autowired
    IOrderService orderService;

    @Autowired
    IUserService userService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView showCheckout(ModelAndView model, @RequestParam int orderId, HttpSession httpSession) {
        ShoppingCart order = orderService.getOrder(orderId);
        if (order == null || !order.getStatus().equals("Не оформлен")) {
            return catalogController.showHome(model, httpSession);
        }
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Customer user = userService.getUser(auth.getName());
        user.setPaymentMethodList(userService.getUserPms(user.getUsername()));

        order.setList(orderService.populateCart(orderId));

        cartController.insertAvailability(order);

        if (order.isHasNotAvailableItems()) {
            model.setViewName("redirect:/cart/");
            return model;
        }

        ArrayList<PaymentMethod> listCrd = orderService.getPaymentMethodsCrd(user.getId());
        ArrayList<PaymentMethod> listPs = orderService.getPaymentMethodsPS(user.getId());

        cartController.insertAvailability(order);

        CheckOutForm form = new CheckOutForm();
        form.setOrderId(order.getId());
        form.setPmRadio("newpm");
        form.setNewPmRadio("card");

        model.addObject("CheckOutForm", form);

        model.addObject("cartsd", order);


        model.addObject("listCrd", listCrd);
        model.addObject("listPs", listPs);

        model.addObject("order", order);
        model.addObject("user", user);
        model.setViewName("checkout");
        return model;
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ModelAndView checkout(CheckOutForm form, ModelAndView model, HttpSession httpSession) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Customer user = userService.getUser(auth.getName());
        user.setPaymentMethodList(userService.getUserPms(user.getUsername()));
        boolean hasErrors = false;

        ShoppingCart sc = orderService.getOrder(form.getOrderId());
        cartController.insertAvailability(sc);

        if (sc.isHasNotAvailableItems()) {
            model.setViewName("redirect:/cart/");
            return model;
        }

        if (sc == null || !sc.getStatus().equals("Не оформлен")) {
            return catalogController.showHome(model, httpSession);
        }
        int pmId = -1;

        if (form.getPmRadio().equals("newpm")) {
            if (form.getNewPmRadio().equals("card")) {
                if (form.getNewCrdExpMonth() == 0 ||
                        form.getNewCrdExpYear() == 0 ||
                        form.getNewCrdName().equals("") ||
                        form.getNewCrdNumber().equals("") ||
                        form.getNewCrdSecNumber().equals("")) {
                    model.addObject("hasErrors", true);
                    model.setViewName("redirect:?orderId=" + form.getOrderId());
                    return model;
                }
                AddCardParameters parameters1 = new AddCardParameters(user.getId(), form.getNewCrdName(), form.getNewCrdExpMonth(), form.getNewCrdExpYear(), form.getNewCrdSecNumber(), form.getNewCrdNumber(), pmId);
                orderService.addCard(parameters1);
                orderService.passOrder(new PassInfoparameters(form.getOrderId(), parameters1.getPm_id()));
            } else if (form.getNewPmRadio().equals("ps")) {
                if (form.getNewPsName().equals("") ||
                        form.getNewPsWallet().equals("")) {
                    model.addObject("hasErrors", true);
                    model.setViewName("redirect:?orderId=" + form.getOrderId());
                    return model;
                }
                AddPsParameters parameters = new AddPsParameters(user.getId(), form.getNewPsName(), form.getNewPsWallet(), pmId);
                orderService.addPs(parameters);
                orderService.passOrder(new PassInfoparameters(form.getOrderId(), parameters.getPm_id()));
            }
        } else if (form.getPmRadio().equals("card")) {
            if (form.getPsMulti().equals("")) {
                model.addObject("hasErrors", true);
                model.setViewName("redirect:?orderId=" + form.getOrderId());
                return model;
            }
            orderService.passOrder(new PassInfoparameters(form.getOrderId(), Integer.parseInt(form.getCrdMulti())));
        } else if (form.getPmRadio().equals("ps")) {
            if (form.getCrdMulti().equals("")) {
                model.addObject("hasErrors", true);
                model.setViewName("redirect:?orderId=" + form.getOrderId());
                return model;
            }
            orderService.passOrder(new PassInfoparameters(form.getOrderId(), Integer.parseInt(form.getPsMulti())));
        }

        model.setViewName("redirect:/orders/");
        return model;
    }
}

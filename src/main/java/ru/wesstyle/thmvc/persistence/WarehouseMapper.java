package ru.wesstyle.thmvc.persistence;

/**
 * Created by wesStyle on 21/03/14.
 */
public interface WarehouseMapper {
    public int getKeys(int id);

    public int getBoxes(int id);
}

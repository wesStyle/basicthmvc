package ru.wesstyle.thmvc.persistence;

import ru.wesstyle.thmvc.domain.Customer;
import ru.wesstyle.thmvc.domain.PaymentMethod;
import ru.wesstyle.thmvc.domain.RegisterUserParameters;

import java.util.List;

/**
 * Created by wesStyle on 20/03/14.
 */
public interface UserMapper {

    public Customer getUser(String username);

    public void registerUser(RegisterUserParameters parameters);

    public List<PaymentMethod> getUserPms(String username);
}

package ru.wesstyle.thmvc.persistence;

import ru.wesstyle.thmvc.domain.software.Category;
import ru.wesstyle.thmvc.domain.software.Edition;
import ru.wesstyle.thmvc.domain.software.Software;

import java.util.ArrayList;

/**
 * Created by wesStyle on 20/03/14.
 */
public interface SoftwareMapper {
    public Software getSoftware(int id);

    public ArrayList<Software> getSoftwareList();

    public ArrayList<Category> getAllSoftwareCategories();

    public ArrayList<Software> getSoftwareByCategoryId(int id);

    public ArrayList<Software> getSoftwareByParentCategory(int id);

    public Edition getEditionById(int id);
}

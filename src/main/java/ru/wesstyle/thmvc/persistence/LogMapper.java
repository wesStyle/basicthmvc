package ru.wesstyle.thmvc.persistence;

import ru.wesstyle.thmvc.domain.OrderLogItem;

import java.util.List;

/**
 * Created by wesStyle on 07/04/14.
 */
public interface LogMapper {
    public List<OrderLogItem> getOrderLogs();

    public int getUsersCount();

    public int getOrdersCount();

    public int getDelayedOrdersCount();

    public int getDoneOrdersCount();
}

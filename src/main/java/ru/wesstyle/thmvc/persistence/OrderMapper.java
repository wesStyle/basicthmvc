package ru.wesstyle.thmvc.persistence;

import ru.wesstyle.thmvc.domain.*;
import ru.wesstyle.thmvc.domain.order.ShoppingCart;
import ru.wesstyle.thmvc.domain.order.ShoppingItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wesStyle on 22/03/14.
 */
public interface OrderMapper {
    public void checkAvailability(AvailabilityParameters parameters);

    public void createSb(CreateSBParameters parameters);

    public void changeAmount(ChangeAmountParameters parameters);

    public void deleteSb(DeleteSbParameters parameters);

    public ShoppingCart getBoxOpenOrders(int userId);

    public ShoppingCart getDigitalOpenOrder(int userId);

    public ArrayList<ShoppingItem> getAllItemsForOrder(int orderId);

    public void createOpenOrders(int uid);

    public ArrayList<PaymentMethod> getPaymentMethodsCrd(int uid);

    public ArrayList<PaymentMethod> getPaymentMethodsPS(int uid);

    public ShoppingCart getOrderById(int orderId);

    public void addCard(AddCardParameters parameters);

    public void addPs(AddPsParameters parameters);

    public void passInfo(PassInfoparameters parameters);

    public ArrayList<ShoppingCart> getAllOrders(int uid);

    public List<Key> getKeys(int orderId, int edId);

    public void processOrders();
}

package ru.wesstyle.thmvc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.wesstyle.thmvc.domain.*;
import ru.wesstyle.thmvc.domain.order.ShoppingCart;
import ru.wesstyle.thmvc.domain.order.ShoppingItem;
import ru.wesstyle.thmvc.persistence.OrderMapper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wesStyle on 22/03/14.
 */

@Service
public class OrderService implements IOrderService {

    @Autowired
    OrderMapper orderMapper;

    @Override
    public int checkAvailability(int edId, int amount) {
        AvailabilityParameters parameters = new AvailabilityParameters();
        parameters.setAmount(amount);
        parameters.setEdId(edId);
        orderMapper.checkAvailability(parameters);
        return parameters.getDifRes();
    }

    @Override
    public ShoppingCart getBoxOpenCart(int uid) {
        return orderMapper.getBoxOpenOrders(uid);
    }

    @Override
    public ShoppingCart getDigitalOpenOrder(int uid) {
        return orderMapper.getDigitalOpenOrder(uid);
    }

    @Override
    public void createOpenOrders(int uid) {
        CreateOpenOrderParameters parameters = new CreateOpenOrderParameters(uid);
        orderMapper.createOpenOrders(uid);
    }

    @Override
    public void addToCart(CreateSBParameters parameters) {
        orderMapper.createSb(parameters);
    }

    @Override
    public void changeCartItem(ChangeAmountParameters parameters) {
        orderMapper.changeAmount(parameters);
    }

    @Override
    public ArrayList<ShoppingItem> populateCart(int orderId) {
        return orderMapper.getAllItemsForOrder(orderId);
    }

    @Override
    public void deleteCartItem(DeleteSbParameters parameters) {
        orderMapper.deleteSb(parameters);
    }

    @Override
    public ShoppingCart getOrder(int id) {
        return orderMapper.getOrderById(id);
    }

    @Override
    public ArrayList<PaymentMethod> getPaymentMethodsPS(int uid) {
        return orderMapper.getPaymentMethodsPS(uid);
    }

    @Override
    public ArrayList<PaymentMethod> getPaymentMethodsCrd(int uid) {
        return orderMapper.getPaymentMethodsCrd(uid);
    }

    @Override
    public void addPs(AddPsParameters parameters) {
        orderMapper.addPs(parameters);
    }

    @Override
    public void addCard(AddCardParameters parameters) {
        orderMapper.addCard(parameters);
    }

    @Override
    public void passOrder(PassInfoparameters parameters) {
        orderMapper.passInfo(parameters);
    }

    @Override
    public ArrayList<ShoppingCart> getAllOrders(int uid) {
        return orderMapper.getAllOrders(uid);
    }

    @Override
    public List<Key> getKeys(int orderId, int edId) {
        return orderMapper.getKeys(orderId, edId);
    }

    @Override
    public void processOrders() {
        orderMapper.processOrders();
    }
}

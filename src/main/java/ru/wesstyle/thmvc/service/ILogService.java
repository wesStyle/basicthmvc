package ru.wesstyle.thmvc.service;

import ru.wesstyle.thmvc.domain.OrderLogItem;

import java.util.List;

/**
 * Created by wesStyle on 07/04/14.
 */
public interface ILogService {
    public List<OrderLogItem> getOrderLogs();

    public int getOrdersCount();

    public int getDelayedOrdersCount();

    public int getDoneOrdersCount();

    public int getUsersCount();
}

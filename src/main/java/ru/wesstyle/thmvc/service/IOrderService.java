package ru.wesstyle.thmvc.service;

import ru.wesstyle.thmvc.domain.*;
import ru.wesstyle.thmvc.domain.order.ShoppingCart;
import ru.wesstyle.thmvc.domain.order.ShoppingItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wesStyle on 22/03/14.
 */
public interface IOrderService {
    public int checkAvailability(int edId, int amount);

    public ShoppingCart getBoxOpenCart(int uid);

    public ShoppingCart getDigitalOpenOrder(int uid);

    public void createOpenOrders(int uid);

    public void addToCart(CreateSBParameters parameters);

    public void changeCartItem(ChangeAmountParameters parameters);

    public ArrayList<ShoppingItem> populateCart(int orderId);

    public void deleteCartItem(DeleteSbParameters parameters);

    public ShoppingCart getOrder(int id);

    public ArrayList<PaymentMethod> getPaymentMethodsPS(int uid);

    public ArrayList<PaymentMethod> getPaymentMethodsCrd(int uid);

    public void addPs(AddPsParameters parameters);

    public void addCard(AddCardParameters parameters);

    public void passOrder(PassInfoparameters parameters);

    public ArrayList<ShoppingCart> getAllOrders(int uid);

    public List<Key> getKeys(int orderId, int edId);

    public void processOrders();
}

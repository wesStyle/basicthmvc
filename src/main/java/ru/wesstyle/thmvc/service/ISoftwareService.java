package ru.wesstyle.thmvc.service;

import ru.wesstyle.thmvc.domain.software.Category;
import ru.wesstyle.thmvc.domain.software.Edition;
import ru.wesstyle.thmvc.domain.software.Software;

import java.util.ArrayList;

/**
 * Created by wesStyle on 20/03/14.
 */
public interface ISoftwareService {
    public Software getSoftware(int id);

    public ArrayList<Software> getSoftwareList();

    public ArrayList<Category> getSoftWareCategories();

    public ArrayList<Software> getSoftwareByCategoryId(int id);

    public ArrayList<Software> getSoftwareByParentCategoryId(int id);

    public Edition getEditionById(int id);
}

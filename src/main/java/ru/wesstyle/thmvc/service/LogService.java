package ru.wesstyle.thmvc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.wesstyle.thmvc.domain.OrderLogItem;
import ru.wesstyle.thmvc.persistence.LogMapper;

import java.util.List;

/**
 * Created by wesStyle on 07/04/14.
 */
@Service
public class LogService implements ILogService {
    @Autowired
    LogMapper logMapper;

    @Override
    public List<OrderLogItem> getOrderLogs() {
        return logMapper.getOrderLogs();
    }

    @Override
    public int getOrdersCount() {
        return logMapper.getOrdersCount();
    }

    @Override
    public int getDelayedOrdersCount() {
        return logMapper.getDelayedOrdersCount();
    }

    @Override
    public int getDoneOrdersCount() {
        return logMapper.getDoneOrdersCount();
    }

    @Override
    public int getUsersCount() {
        return logMapper.getUsersCount();
    }
}

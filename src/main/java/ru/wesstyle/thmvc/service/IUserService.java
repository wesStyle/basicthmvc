package ru.wesstyle.thmvc.service;

import ru.wesstyle.thmvc.domain.Customer;
import ru.wesstyle.thmvc.domain.PaymentMethod;
import ru.wesstyle.thmvc.domain.RegisterUserParameters;

import java.util.List;

/**
 * Created by wesStyle on 24/03/14.
 */
public interface IUserService {
    public Customer getUser(String username);

    public void regUser(RegisterUserParameters parameters);

    public List<PaymentMethod> getUserPms(String username);
}

package ru.wesstyle.thmvc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.wesstyle.thmvc.domain.Customer;
import ru.wesstyle.thmvc.domain.PaymentMethod;
import ru.wesstyle.thmvc.domain.RegisterUserParameters;
import ru.wesstyle.thmvc.persistence.UserMapper;

import java.util.List;

/**
 * Created by wesStyle on 24/03/14.
 */
@Service
public class UserService implements IUserService {
    @Autowired
    UserMapper userMapper;

    @Override
    public Customer getUser(String username) {
        return userMapper.getUser(username);
    }

    @Override
    public void regUser(RegisterUserParameters parameters) {
        userMapper.registerUser(parameters);
    }

    @Override
    public List<PaymentMethod> getUserPms(String username) {
        return userMapper.getUserPms(username);
    }
}

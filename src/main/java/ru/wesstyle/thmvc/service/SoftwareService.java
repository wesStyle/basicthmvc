package ru.wesstyle.thmvc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.wesstyle.thmvc.domain.software.Category;
import ru.wesstyle.thmvc.domain.software.Edition;
import ru.wesstyle.thmvc.domain.software.Software;
import ru.wesstyle.thmvc.persistence.SoftwareMapper;
import ru.wesstyle.thmvc.persistence.WarehouseMapper;
import ru.wesstyle.thmvc.util.CategoryUtil;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by wesStyle on 20/03/14.
 */
@Service
public class SoftwareService implements ISoftwareService {
    @Autowired
    SoftwareMapper softwareMapper;

    @Autowired
    WarehouseMapper warehouseMapper;

    ArrayList<Software> soft;

    @Override
    public Software getSoftware(int id) {
        Software sft = softwareMapper.getSoftware(id);
        for (Iterator<Edition> j = sft.getEditions().iterator(); j.hasNext(); ) {
            Edition edit = j.next();
            edit.setAvailability(edit.getEditionType() == "Коробочное" ? warehouseMapper.getBoxes(edit.getId()) : warehouseMapper.getKeys(edit.getId()));
        }
        return sft;
    }

    @Override
    public ArrayList<Software> getSoftwareList() {
        soft = softwareMapper.getSoftwareList();
        insertAvailability(soft);
        return soft;
    }

    @Override
    public ArrayList<Category> getSoftWareCategories() {
        return CategoryUtil.getCategoryTree(softwareMapper.getAllSoftwareCategories());
    }

    @Override
    public ArrayList<Software> getSoftwareByCategoryId(int id) {
        soft = softwareMapper.getSoftwareByCategoryId(id);
        insertAvailability(soft);
        return soft;
    }

    @Override
    public ArrayList<Software> getSoftwareByParentCategoryId(int id) {
        soft = softwareMapper.getSoftwareByParentCategory(id);
        insertAvailability(soft);
        return soft;
    }

    @Override
    public Edition getEditionById(int id) {
        Edition ed = softwareMapper.getEditionById(id);
        if (ed != null)
            ed.setAvailability(ed.getEditionType() == "Коробочное" ? warehouseMapper.getBoxes(ed.getId()) : warehouseMapper.getKeys(ed.getId()));
        return ed;
    }

    void insertAvailability(ArrayList<Software> list) {
        for (Iterator<Software> i = list.iterator(); i.hasNext(); ) {
            Software sft = i.next();
            for (Iterator<Edition> j = sft.getEditions().iterator(); j.hasNext(); ) {
                Edition edit = j.next();
                edit.setAvailability(edit.getEditionType() == "Коробочное" ? warehouseMapper.getBoxes(edit.getId()) : warehouseMapper.getKeys(edit.getId()));
            }
        }
    }
}

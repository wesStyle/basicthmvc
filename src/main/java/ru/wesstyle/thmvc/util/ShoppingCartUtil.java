package ru.wesstyle.thmvc.util;

import ru.wesstyle.thmvc.domain.order.ShoppingCart;
import ru.wesstyle.thmvc.domain.order.ShoppingItem;
import ru.wesstyle.thmvc.web.EditionAndAmount;

import java.util.Iterator;
import java.util.List;

/**
 * Created by wesStyle on 22/03/14.
 */
public class ShoppingCartUtil {
    public static void refreshCart(ShoppingCart shoppingCart, List<EditionAndAmount> amounts) {
        for (Iterator<ShoppingItem> iterator = shoppingCart.getList().iterator(); iterator.hasNext(); ) {
            ShoppingItem item = iterator.next();
            for (Iterator<EditionAndAmount> amnts = amounts.iterator(); amnts.hasNext(); ) {
                EditionAndAmount ed = amnts.next();
                if (item.getEd().getId() == ed.getEdId()) {
                    if (item.getAmount() != ed.getAmount()) {
                        if (ed.getAmount() == 0) {
                            iterator.remove();
                            shoppingCart.calculatePrice();
                        } else {
                            item.setAmount(ed.getAmount());
                            shoppingCart.calculatePrice();
                        }
                    }
                }
            }
        }


    }
}

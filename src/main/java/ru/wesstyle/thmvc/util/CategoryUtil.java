package ru.wesstyle.thmvc.util;

import ru.wesstyle.thmvc.domain.software.Category;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by wesStyle on 21/03/14.
 */
public class CategoryUtil {
    public static ArrayList<Category> getCategoryTree(ArrayList<Category> plainCats) {
        ArrayList<Category> tree = new ArrayList<Category>();

        for (Iterator<Category> i = plainCats.iterator(); i.hasNext(); ) {
            Category item = i.next();
            if (item.getParentCategoryId() == null) {
                tree.add(item);
            }
        }

        for (Iterator<Category> i = tree.iterator(); i.hasNext(); ) {
            Category parent = i.next();
            for (Iterator<Category> j = plainCats.iterator(); j.hasNext(); ) {
                Category item = j.next();
                if (item.getParentCategoryId() == parent.getId()) parent.getChildren().add(item);
            }
        }

        return tree;
    }

    public static Category isParentCategoryExist(int pid, ArrayList<Category> cats) {
        Category pcat;
        for (Iterator<Category> i = cats.iterator(); i.hasNext(); ) {
            pcat = i.next();
            if (pcat.getId() == pid) {
                return pcat;
            }
        }
        return null;
    }

    public static Category isCategoryExist(int catId, ArrayList<Category> cats) {
        Category cat;
        for (Iterator<Category> i = cats.iterator(); i.hasNext(); ) {
            Category par = i.next();
            for (Iterator<Category> j = par.getChildren().iterator(); j.hasNext(); ) {
                cat = j.next();
                if (cat.getId() == catId) {
                    return cat;
                }
            }
        }
        return null;
    }
}

package ru.wesstyle.thmvc.domain;

import java.io.Serializable;

/**
 * Created by wesStyle on 24/03/14.
 */
public class AddCardParameters implements Serializable {
    private static final long serialVersionUID = -6175964769058886658L;

    public AddCardParameters() {
    }

    public AddCardParameters(int u_id, String crd_name, int exp_month, int exp_year, String sec_code, String numb, int pm_id) {
        this.u_id = u_id;
        this.crd_name = crd_name;
        this.exp_month = exp_month;
        this.exp_year = exp_year;
        this.sec_code = sec_code;
        this.numb = numb;
        this.pm_id = pm_id;
    }

    int u_id;
    String crd_name;
    int exp_month;
    int exp_year;
    String sec_code;
    String numb;
    int pm_id;

    public int getU_id() {
        return u_id;
    }

    public void setU_id(int u_id) {
        this.u_id = u_id;
    }

    public String getCrd_name() {
        return crd_name;
    }

    public void setCrd_name(String crd_name) {
        this.crd_name = crd_name;
    }

    public int getExp_month() {
        return exp_month;
    }

    public void setExp_month(int exp_month) {
        this.exp_month = exp_month;
    }

    public int getExp_year() {
        return exp_year;
    }

    public void setExp_year(int exp_year) {
        this.exp_year = exp_year;
    }

    public String getSec_code() {
        return sec_code;
    }

    public void setSec_code(String sec_code) {
        this.sec_code = sec_code;
    }

    public String getNumb() {
        return numb;
    }

    public void setNumb(String numb) {
        this.numb = numb;
    }

    public int getPm_id() {
        return pm_id;
    }

    public void setPm_id(int pm_id) {
        this.pm_id = pm_id;
    }
}

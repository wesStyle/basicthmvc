package ru.wesstyle.thmvc.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by wesStyle on 24/03/14.
 */
public class Customer implements Serializable {
    private static final long serialVersionUID = -118710736587053663L;
    int id;
    String username;
    String name;
    String surname;
    String email;
    List<PaymentMethod> paymentMethodList = new ArrayList<PaymentMethod>();

    public String getUsername() {
        return username;
    }

    public List<PaymentMethod> getPaymentMethodList() {
        return paymentMethodList;
    }

    public void setPaymentMethodList(List<PaymentMethod> paymentMethodList) {
        this.paymentMethodList = paymentMethodList;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}

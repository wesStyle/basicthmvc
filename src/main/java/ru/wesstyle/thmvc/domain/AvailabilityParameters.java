package ru.wesstyle.thmvc.domain;

import java.io.Serializable;

/**
 * Created by wesStyle on 22/03/14.
 */
public class AvailabilityParameters implements Serializable {
    private static final long serialVersionUID = 6382052299631159435L;
    int edId;
    int amount;
    int difRes;

    public AvailabilityParameters() {
    }

    public AvailabilityParameters(int edId, int amount, int difRes) {
        this.edId = edId;
        this.amount = amount;
        this.difRes = difRes;
    }

    public int getEdId() {
        return edId;
    }

    public void setEdId(int edId) {
        this.edId = edId;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getDifRes() {
        return difRes;
    }

    public void setDifRes(int difRes) {
        this.difRes = difRes;
    }
}

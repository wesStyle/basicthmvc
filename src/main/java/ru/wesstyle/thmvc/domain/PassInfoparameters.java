package ru.wesstyle.thmvc.domain;

import java.io.Serializable;

/**
 * Created by wesStyle on 24/03/14.
 */
public class PassInfoparameters implements Serializable {
    private static final long serialVersionUID = 4820794589789078142L;

    public PassInfoparameters() {
    }

    public PassInfoparameters(int orderId, int pmId) {
        this.orderId = orderId;
        this.pmId = pmId;
    }

    int orderId;
    int pmId;

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getPmId() {
        return pmId;
    }

    public void setPmId(int pmId) {
        this.pmId = pmId;
    }
}

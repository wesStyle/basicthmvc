package ru.wesstyle.thmvc.domain;

import java.io.Serializable;

/**
 * Created by wesStyle on 24/03/14.
 */

public class AddPsParameters implements Serializable {
    private static final long serialVersionUID = 4887222577193474470L;

    public AddPsParameters() {
    }

    public AddPsParameters(int u_id, String ps_name, String wallet_numb, int pm_id) {
        this.u_id = u_id;
        this.ps_name = ps_name;
        this.wallet_numb = wallet_numb;
        this.pm_id = pm_id;
    }

    int u_id;
    String ps_name;
    String wallet_numb;
    int pm_id;

    public int getU_id() {
        return u_id;
    }

    public void setU_id(int u_id) {
        this.u_id = u_id;
    }

    public String getPs_name() {
        return ps_name;
    }

    public void setPs_name(String ps_name) {
        this.ps_name = ps_name;
    }

    public String getWallet_numb() {
        return wallet_numb;
    }

    public void setWallet_numb(String wallet_numb) {
        this.wallet_numb = wallet_numb;
    }

    public int getPm_id() {
        return pm_id;
    }

    public void setPm_id(int pm_id) {
        this.pm_id = pm_id;
    }
}

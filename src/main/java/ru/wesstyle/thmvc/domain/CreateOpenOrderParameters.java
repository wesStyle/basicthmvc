package ru.wesstyle.thmvc.domain;

import java.io.Serializable;

/**
 * Created by wesStyle on 24/03/14.
 */
public class CreateOpenOrderParameters implements Serializable {
    private static final long serialVersionUID = -4359199931682775310L;

    int uid;

    public CreateOpenOrderParameters() {
    }

    public CreateOpenOrderParameters(int uid) {
        this.uid = uid;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }
}

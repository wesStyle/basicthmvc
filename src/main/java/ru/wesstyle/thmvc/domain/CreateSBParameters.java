package ru.wesstyle.thmvc.domain;

import java.io.Serializable;

/**
 * Created by wesStyle on 24/03/14.
 */
public class CreateSBParameters implements Serializable {
    private static final long serialVersionUID = 3180591810770231533L;
    int uid;
    int edId;
    int amount;

    public CreateSBParameters() {
    }

    public CreateSBParameters(int uid, int edId, int amount) {
        this.uid = uid;
        this.edId = edId;
        this.amount = amount;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public int getEdId() {
        return edId;
    }

    public void setEdId(int edId) {
        this.edId = edId;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}

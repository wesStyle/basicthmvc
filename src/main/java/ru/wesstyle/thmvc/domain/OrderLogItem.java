package ru.wesstyle.thmvc.domain;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by wesStyle on 07/04/14.
 */
public class OrderLogItem implements Serializable {
    private static final long serialVersionUID = -8614292960469540770L;

    int id;
    int orderId;
    String orderStatus;
    String orderPrStatus;
    Date timeStamp;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getOrderPrStatus() {
        return orderPrStatus;
    }

    public void setOrderPrStatus(String orderPrStatus) {
        this.orderPrStatus = orderPrStatus;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }
}

package ru.wesstyle.thmvc.domain;

/**
 * Created by wesStyle on 07/04/14.
 */
public class RegisterUserParameters {
    String user_login, user_name, user_surname, user_email, user_password, user_role;
    Integer u_id;

    public RegisterUserParameters() {
    }

    public RegisterUserParameters(String user_login, String user_name, String user_surname, String user_email, String user_password, String user_role, Integer u_id) {
        this.user_login = user_login;
        this.user_name = user_name;
        this.user_surname = user_surname;
        this.user_email = user_email;
        this.user_password = user_password;
        this.user_role = user_role;
        this.u_id = u_id;
    }

    public String getUser_login() {
        return user_login;
    }

    public void setUser_login(String user_login) {
        this.user_login = user_login;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_surname() {
        return user_surname;
    }

    public void setUser_surname(String user_surname) {
        this.user_surname = user_surname;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getUser_password() {
        return user_password;
    }

    public void setUser_password(String user_password) {
        this.user_password = user_password;
    }

    public String getUser_role() {
        return user_role;
    }

    public void setUser_role(String user_role) {
        this.user_role = user_role;
    }

    public Integer getU_id() {
        return u_id;
    }

    public void setU_id(Integer u_id) {
        this.u_id = u_id;
    }
}

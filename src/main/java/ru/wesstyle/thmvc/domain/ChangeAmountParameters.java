package ru.wesstyle.thmvc.domain;

import java.io.Serializable;

/**
 * Created by wesStyle on 24/03/14.
 */
public class ChangeAmountParameters implements Serializable {

    private static final long serialVersionUID = 8969620305849079313L;

    int sbId;
    int edId;
    int amt;
    int orderId;

    public ChangeAmountParameters() {
    }

    ;

    public ChangeAmountParameters(int sbId, int edId, int amt, int orderId) {
        this.sbId = sbId;
        this.edId = edId;
        this.amt = amt;
        this.orderId = orderId;
    }

    public int getSbId() {
        return sbId;
    }

    public void setSbId(int sbId) {
        this.sbId = sbId;
    }

    public int getEdId() {
        return edId;
    }

    public void setEdId(int edId) {
        this.edId = edId;
    }

    public int getAmt() {
        return amt;
    }

    public void setAmt(int amt) {
        this.amt = amt;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }
}

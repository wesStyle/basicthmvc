package ru.wesstyle.thmvc.domain;

import java.io.Serializable;

/**
 * Created by wesStyle on 24/03/14.
 */
public class PayCard implements Serializable {
    private static final long serialVersionUID = 4995550928851189147L;
    int id;
    String securityCode;
    String number;
    int expMonth;
    int expYear;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getSecurityCode() {
        return securityCode;
    }

    public void setSecurityCode(String securityCode) {
        this.securityCode = securityCode;
    }

    public int getExpMonth() {
        return expMonth;
    }

    public void setExpMonth(int expMonth) {
        this.expMonth = expMonth;
    }

    public int getExpYear() {
        return expYear;
    }

    public void setExpYear(int expYear) {
        this.expYear = expYear;
    }
}

package ru.wesstyle.thmvc.domain.software.attributes;

import java.io.Serializable;

/**
 * Created by wesStyle on 21/03/14.
 */
public class Activation implements Serializable {
    private static final long serialVersionUID = -2444914338636991108L;
    int id;
    int period;
    int amount;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}

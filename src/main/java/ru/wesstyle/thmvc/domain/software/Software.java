package ru.wesstyle.thmvc.domain.software;

import ru.wesstyle.thmvc.domain.software.attributes.Language;
import ru.wesstyle.thmvc.domain.software.attributes.Platform;
import ru.wesstyle.thmvc.domain.software.attributes.Publisher;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by wesStyle on 20/03/14.
 */
public class Software implements Serializable {
    private static final long serialVersionUID = 7326727729264947992L;

    int id;
    String name;
    Date releaseDate;
    Publisher publisher;
    Category category;
    Category parentCategory;
    String desc;
    List<Language> langs = new ArrayList<Language>();
    List<Platform> platforms = new ArrayList<Platform>();
    List<Edition> editions = new ArrayList<Edition>();

    public List<Edition> getEditions() {
        return editions;
    }

    public void setEditions(List<Edition> editions) {
        this.editions = editions;
    }

    public List<Platform> getPlatforms() {
        return platforms;
    }

    public void setPlatforms(List<Platform> platforms) {
        this.platforms = platforms;
    }

    public List<Language> getLangs() {
        return langs;
    }

    public void setLangs(List<Language> langs) {
        this.langs = langs;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Category getParentCategory() {
        return parentCategory;
    }

    public void setParentCategory(Category parentCategory) {
        this.parentCategory = parentCategory;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public Publisher getPublisher() {
        return publisher;
    }

    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}

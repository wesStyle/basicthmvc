package ru.wesstyle.thmvc.domain.software;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by wesStyle on 20/03/14.
 */
public class Category implements Serializable {
    private static final long serialVersionUID = -569917384435370565L;

    Integer id;
    String name;
    Integer parentCategoryId;
    List<Category> children = new ArrayList<Category>();

    public Integer getParentCategoryId() {
        return parentCategoryId;
    }

    public void setParentCategoryId(Integer parentCategoryId) {
        this.parentCategoryId = parentCategoryId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Category> getChildren() {
        return children;
    }

    public void setChildren(List<Category> children) {
        this.children = children;
    }
}

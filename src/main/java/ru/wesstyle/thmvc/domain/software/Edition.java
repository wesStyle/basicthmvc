package ru.wesstyle.thmvc.domain.software;

import ru.wesstyle.thmvc.domain.software.attributes.Activation;

import java.io.Serializable;

/**
 * Created by wesStyle on 21/03/14.
 */
public class Edition implements Serializable {
    private static final long serialVersionUID = -4997813858248508771L;

    int id;
    String extras;
    int price;
    String editionType;
    String name;
    Activation activation;
    int availability;
    String softName;

    public String getSoftName() {
        return softName;
    }

    public void setSoftName(String softName) {
        this.softName = softName;
    }

    public int getAvailability() {
        return availability;
    }

    public void setAvailability(int availability) {
        this.availability = availability;
    }

    public Activation getActivation() {
        return activation;
    }

    public void setActivation(Activation activation) {
        this.activation = activation;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getExtras() {
        return extras;
    }

    public void setExtras(String extras) {
        this.extras = extras;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getEditionType() {
        return editionType;
    }

    public void setEditionType(String editionType) {
        this.editionType = editionType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

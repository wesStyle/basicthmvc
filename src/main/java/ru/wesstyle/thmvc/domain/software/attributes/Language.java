package ru.wesstyle.thmvc.domain.software.attributes;

import java.io.Serializable;

/**
 * Created by wesStyle on 21/03/14.
 */
public class Language implements Serializable {
    private static final long serialVersionUID = -5794750081946173980L;

    String name;
    int id;

    public String getName() {

        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}

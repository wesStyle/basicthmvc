package ru.wesstyle.thmvc.domain.software.attributes;

import java.io.Serializable;

/**
 * Created by wesStyle on 21/03/14.
 */
public class Platform implements Serializable {
    private static final long serialVersionUID = 7910907951428741856L;

    int id;
    String name;
    int version;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
}

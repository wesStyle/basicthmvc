package ru.wesstyle.thmvc.domain.software.attributes;

import java.io.Serializable;

/**
 * Created by wesStyle on 20/03/14.
 */
public class Publisher implements Serializable {
    private static final long serialVersionUID = -1873667227103537747L;

    int id;
    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}

package ru.wesstyle.thmvc.domain;

import java.io.Serializable;

/**
 * Created by wesStyle on 24/03/14.
 */
public class DeleteSbParameters implements Serializable {
    private static final long serialVersionUID = 6523728720881466063L;

    int sbId;
    int orderId;

    public DeleteSbParameters() {
    }

    public DeleteSbParameters(int sbId, int orderId) {
        this.sbId = sbId;
        this.orderId = orderId;
    }

    public int getSbId() {
        return sbId;
    }

    public void setSbId(int sbId) {
        this.sbId = sbId;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }
}

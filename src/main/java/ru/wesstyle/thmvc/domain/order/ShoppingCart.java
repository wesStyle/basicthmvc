package ru.wesstyle.thmvc.domain.order;

import ru.wesstyle.thmvc.domain.PaymentMethod;
import ru.wesstyle.thmvc.domain.software.Edition;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Created by wesStyle on 21/03/14.
 */
public class ShoppingCart implements Serializable {
    private static final long serialVersionUID = -7133380584238717595L;

    int id;
    String type;
    int userId;
    Date orderDate;
    String status;
    List<ShoppingItem> list = new ArrayList<ShoppingItem>();
    PaymentMethod paymentMethod;
    Integer pmId;
    int price;
    int count;

    boolean hasNotAvailableItems;

    public boolean isHasNotAvailableItems() {
        return hasNotAvailableItems;
    }

    public void setHasNotAvailableItems(boolean hasNotAvailableItems) {
        this.hasNotAvailableItems = hasNotAvailableItems;
    }

    public int getPmId() {
        return pmId;
    }

    public void setPmId(int pmId) {
        this.pmId = pmId;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public List<ShoppingItem> getList() {
        return list;
    }

    public int getPrice() {
        return price;
    }

    public void setCount() {
        count = 0;
        if (list.isEmpty()) count = 0;
        else
            for (Iterator<ShoppingItem> itemIterator = list.iterator(); itemIterator.hasNext(); ) {
                ShoppingItem item = itemIterator.next();
                count += item.amount;
            }
    }

    public int getCount() {
        setCount();
        return count;
    }

    public void setList(List<ShoppingItem> list) {
        this.list = list;
    }

    public void addShoppingItem(Edition edition, int amount) {
        boolean hasEdition = false;
        for (Iterator<ShoppingItem> itemIterator = list.iterator(); itemIterator.hasNext(); ) {
            ShoppingItem item = itemIterator.next();
            if (item.getEd().getId() == edition.getId()) {
                item.amount++;
                hasEdition = true;
                break;
            }
        }
        if (!hasEdition) list.add(new ShoppingItem(edition, amount));
        calculatePrice();
    }

    public void removeShoppingItem(Edition edition) {
        for (Iterator<ShoppingItem> itemIterator = list.iterator(); itemIterator.hasNext(); ) {
            ShoppingItem shoppingItem = itemIterator.next();
            if (shoppingItem.getEd().getId() == edition.getId()) {
                itemIterator.remove();
                calculatePrice();
                break;
            }
        }
    }

    public int getItemId(Edition ed) {
        for (Iterator<ShoppingItem> iterator = list.iterator(); iterator.hasNext(); ) {
            ShoppingItem item = iterator.next();
            if (item.getEd().getId() == ed.getId()) return item.getId();
        }
        return 0;
    }

    public ShoppingItem getItemByEdId(int edId) {
        for (Iterator<ShoppingItem> iterator = list.iterator(); iterator.hasNext(); ) {
            ShoppingItem item = iterator.next();
            if (item.getEd().getId() == edId) return item;
        }
        return null;
    }

    public boolean hasItem(Edition ed) {
        for (Iterator<ShoppingItem> iterator = list.iterator(); iterator.hasNext(); ) {
            ShoppingItem item = iterator.next();
            if (item.getEd().getId() == ed.getId()) return true;
        }
        return false;
    }

    public void calculatePrice() {
        price = 0;
        for (Iterator<ShoppingItem> itemIterator = list.iterator(); itemIterator.hasNext(); ) {
            ShoppingItem item = itemIterator.next();
            price += item.ed.getPrice() * item.amount;
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public void setCount(int count) {
        this.count = count;
    }
}

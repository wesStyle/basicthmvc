package ru.wesstyle.thmvc.domain.order;

import ru.wesstyle.thmvc.domain.Key;
import ru.wesstyle.thmvc.domain.software.Edition;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class ShoppingItem implements Serializable {
    private static final long serialVersionUID = -1599101383212410602L;

    int id;
    int userId;
    Date dateAdded;
    Edition ed;
    int amount;
    int available;
    List<Key> keys;

    public List<Key> getKeys() {
        return keys;
    }

    public void setKeys(List<Key> keys) {
        this.keys = keys;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAvailable() {
        return available;
    }

    public void setAvailable(int available) {
        this.available = available;
    }

    public ShoppingItem() {

    }

    public ShoppingItem(Edition edition, int amount) {
        this.amount = amount;
        this.ed = edition;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Edition getEd() {
        return ed;
    }

    public void setEd(Edition ed) {
        this.ed = ed;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Date getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(Date dateAdded) {
        this.dateAdded = dateAdded;
    }
}
package ru.wesstyle.thmvc.domain;

import java.io.Serializable;

/**
 * Created by wesStyle on 24/03/14.
 */
public class PaymentMethod implements Serializable {
    private static final long serialVersionUID = -6811160778395867011L;
    int id;
    String paymentType;
    PayCard card;
    PaySystem paySystem;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public PayCard getCard() {
        return card;
    }

    public void setCard(PayCard card) {
        this.card = card;
    }

    public PaySystem getPaySystem() {
        return paySystem;
    }

    public void setPaySystem(PaySystem paySystem) {
        this.paySystem = paySystem;
    }
}

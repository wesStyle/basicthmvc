var links = document.getElementsByClassName("inputs");
var inputIds = [].map.call(links, function (el) {
    return el.getAttribute('id');
});
var inputValues = [].map.call(links, function (el) {
    return el.getAttribute('value');
});

var typesLinks = document.getElementsByClassName("edType");
var types = [].map.call(typesLinks, function (el) {
    return el.innerText;
});

function jsonForm() {
    $("#logform").ajaxForm(function (json) {
        if (json.status == 'SUCCESS') {
            window.location.replace("/");
        }
        else if (json.status == 'FAIL') {
            $('#modalnest').replaceWith(json.html);
            jsonForm();
        }
        else {
            alert("error");
        }
    });
}

$(document).ready(function () {
    jsonForm();
});

function addToValue(data) {
    var id = '#ed' + data;
    var val = $(id).val();
    val++;
    $(id).val(val);
    inputValues[inputIds.indexOf(id.slice(1))] = val;
}

function removeFromValue(data) {
    var id = '#ed' + data;
    var val = $(id).val();
    val--;
    $(id).val(val);
    if ($(id).val() < 1) {
        $(id).val(1);
        inputValues[inputIds.indexOf(id.slice(1))] = 1;
        return;
    }
    inputValues[inputIds.indexOf(id.slice(1))] = val;

}

function removeFromCartMark(data) {
    var id = '#rmed' + data;
    var trId = '#edtr' + data;

    isActive = $(id).hasClass('active')
    if (!isActive) {
        $(id).addClass('active');
        $(trId).addClass('danger');
    }
    else {
        $(id).removeClass('active');
        $(trId).removeClass('danger');
    }
}

function refresh() {
    var data = [];
    var i = 0;
    var val;
    for (i = 0; i < inputValues.length; i++) {
        if ($('#edtr' + inputIds[i].slice(2)).hasClass("danger")) val = 0;
        else val = inputValues[i];
        data[i] = {edId: inputIds[i].slice(2), amount: val, type: types[i]};
    }

    $.ajax({
        url: '/cart/ref',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        data: JSON.stringify(data),
        success: function (result) {
            if (result.status == "SUCCESS") {
                window.location.replace("/cart/");
            }
            else alert("Возникла ошибка");
        }
    })
}

$('#refreshCart').click(function refreshCart() {
        refresh();
    }
);

$('#refreshCartd').click(function refreshCart() {
        refresh();
    }
);


function addToCart(data, type) {

    var eaa = {
        edId: data,
        amount: 1,
        type: type
    }

    $.ajax({
        url: '/cart/',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        data: JSON.stringify(eaa),
        success: function (result) {
            if (result.status == "SUCCESS") {
                $('#cart-a').replaceWith(result.html);
            }
            else alert("Возникла ошибка");
        }
    });
};